import React, { Component } from "react";
import { Image, TouchableOpacity, SafeAreaView, View, Text, Dimensions, ScrollView, StyleSheet } from "react-native";
import { CustomHeader } from "./CustomHeader";
import { Loader } from "./Loader";
import { SimpleAnimation } from 'react-native-simple-animations';
import { Fab } from "native-base";
import config from "../config/config";
import Toast from 'react-native-tiny-toast';
import ResponsiveSize from "../config/ResponsiveSize";
import { FlatList } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BottomTab from "./BottomTab";
import DetailScreen from "./DetailScreen";
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
export default class HomeScreen extends Component {
    constructor() {
        super();
        this.state = {
            Loader: false,
            Token: "",
            InstituteCode: "",
            active: "home",
            isValidUser: false,
            BannerArray: [
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Student Profile" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Subjects" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Time Table" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Attendance" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Internal Marks" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "University Marks" },
                { image: "https://i.stack.imgur.com/9WYxT.png", name: "Performance" },
            ],
        }
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "white" }}>
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                {/*{this.state.isValidUser ?*/}
                <SafeAreaView style={{ flex: 1 }}>
                    <CustomHeader
                        isHome={null}
                        title="Homescreen"
                        notification={true}
                        navigation={this.props.navigation}
                    />
                    {/*<View style={{ flex: 1 }}>
                        <ScrollView>
                            <Text style={{ fontSize: ResponsiveSize(18), padding: 15 }}>Hey,John</Text>
                        </ScrollView>
                    </View>*/}
                    <FlatList
                        data={this.state.BannerArray}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate("DetailScreen", { title: item.name })
                            }} style={styles.FlatlistChildView}>
                                <Image style={styles.ChildViewImage} source={{ uri: item.image }} />
                                <Text style={styles.ChildViewText}>{item.name}</Text>
                            </TouchableOpacity>
                        )}
                        //Setting the number of column
                        numColumns={2}
                        keyExtractor={(item, index) => index}
                    />

                    <BottomTab
                        active="home"
                        navigation={this.props.navigation}
                    />
                </SafeAreaView>
                {/*:
                    <SafeAreaView style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                        <View style={{ width: "80%", backgroundColor: "white", justifyContent: "space-evenly", borderRadius: 10, elevation: 5, shadowColor: "black", shadowOffset: { width: 1, height: 1 }, shadowOpacity: .3, minHeight: 150, maxHeight: 200 }}>
                            <View style={{ width: "90%", alignSelf: "center", justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ fontSize: ResponsiveSize(16), textAlign: "center", fontWeight: "500" }}>Your number is not registered with us</Text>
                            </View>
                            <TouchableOpacity style={{ width: "50%", height: 45, alignSelf: "center", backgroundColor: "red", borderRadius: 5, justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ color: "white", fontSize: ResponsiveSize(config.buttonSize), fontWeight: "600" }}>Exit</Text>
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>
                }*/}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    FlatlistChildView: {
        width: "45%",
        height: 100,
        borderRadius: 10,
        flexDirection: 'column',
        backgroundColor: "#383E7B",
        margin: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    ChildViewText: {
        fontSize: ResponsiveSize(16),
        color: "white",
        fontWeight: "500"
    },
    ChildViewImage: {
        justifyContent: 'center',
        alignItems: 'center',
        height: "100%",
        width: "100%",
        position: "absolute",
        borderRadius: 10,
    },
})