// React Native App Intro Slider using AppIntroSlider
// https://aboutreact.com/react-native-app-intro-slider/
// Simple Intro Slider

// import React in our code
import React, { useState } from 'react';

// import all the components we are going to use
import {
    SafeAreaView,
    StyleSheet,
    View,
    Text,
    Image,
    Button,
    TouchableOpacity
} from 'react-native';
import { Tooltip } from 'react-native-elements';

import { SimpleAnimation } from 'react-native-simple-animations';
//import AppIntroSlider to use it
import AppIntroSlider from 'react-native-app-intro-slider';

const AppIntro = (props) => {
    const [showRealApp, setShowRealApp] = useState(false);

    const onDone = () => {
        setShowRealApp(true);
    };
    const onSkip = () => {
        setShowRealApp(true);
    };
    _renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Image style={{ width: 30, height: 30, resizeMode: "contain" }} source={require("../assets/right.png")}></Image>

            </View>
        );
    };
    _renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle2}>
                <Image style={{ width: 24, height: 24, resizeMode: "contain" }} source={require("../assets/tick.png")}></Image>
            </View>
        );
    };
    const RenderItem = ({ item, index }) => {
        return (
            //<View
            //    style={{
            //        flex: 1,
            //        backgroundColor: item.backgroundColor,
            //        alignItems: 'center',
            //        justifyContent: 'space-around',
            //        paddingBottom: 100,
            //    }}>
            //    {/*<Text style={styles.introTitleStyle}>
            //        {item.title}
            //    </Text>*/}
            //    <Image
            //        style={styles.introImageStyle}
            //        source={item.image} />
            //    <Text style={styles.introTextStyle}>
            //        {item.text}
            //    </Text>
            //</View>
            <SafeAreaView key={index} style={styles.container}>
                <Image style={{ width: "100%", position: "absolute", height: "100%", alignSelf: "center", resizeMode: "contain" }} source={item.image}>
                </Image>
                <View style={{ minHeight: "25%", width: "100%" }}>
                    <Text style={{ fontSize: 40, textAlign: "center", paddingHorizontal: 20 }}>{item.text}</Text>
                </View>
                {/*<TouchableOpacity style={styles.loginButton} onPress={() => {
                        props.navigation.navigate("HomeStack2")
                    }}>
                        <Text style={styles.text}>GET STARTED</Text>
                    </TouchableOpacity>*/}
            </SafeAreaView>
        );
    };

    return (
        <>
            {showRealApp ? (

                //<SafeAreaView style={styles.container}>
                //    <Image style={{ width: "100%", position: "absolute", height: "100%", alignSelf: "center", resizeMode: "contain" }} source={require('../assets/6.png')}>
                //    </Image>
                //    <Text style={{ fontSize: 45, marginBottom: 100 }}>Take A Photo</Text>
                //    <TouchableOpacity style={styles.loginButton} onPress={() => {
                //        props.navigation.navigate("HomeStack2")
                //    }}>
                //        <Text style={styles.text}>GET STARTED</Text>
                //    </TouchableOpacity>
                //</SafeAreaView>
                <View style={{ flex: 1, backgroundColor: "white" }}>

                    <Image style={{ width: "100%", position: "absolute", height: "100%", marginTop: 40, alignSelf: "center", resizeMode: "contain" }} source={require('../assets/2.png')}>
                    </Image>
                    <SafeAreaView style={{ flex: 1 }}>
                        {/*<CustomHeader
                        isHome={false}
                        navigation={this.props.navigation}
                    />*/}

                        <View style={{ flex: 1, justifyContent: "flex-end", alignItems: "center" }}>
                            {/*<Tooltip withOverlay={true} popover={<Text>Info here</Text>}>
                                <Text>press here</Text>
                            </Tooltip>*/}
                            <SimpleAnimation delay={100} duration={500} direction="up" fade staticType="zoom">
                                <TouchableOpacity onPress={() => {
                                    props.navigation.navigate("QRScanner")
                                    //this.setState({ Loader: !this.state.Loader })
                                }} style={{ marginBottom: 50 }}>
                                    <Image source={require("../assets/qr_scan.png")} style={{ width: 100, height: 100, resizeMode: "contain" }} ></Image>
                                </TouchableOpacity>
                            </SimpleAnimation>

                        </View>
                        {/*<Text style={{ fontSize: 40, color: "white" }}>Homescreen</Text>*/}
                    </SafeAreaView>
                </View>
            ) : (
                    <AppIntroSlider
                        dotClickEnabled={true}
                        activeDotStyle={{ backgroundColor: "#34ABDF" }}
                        data={slides}
                        renderItem={RenderItem}
                        onDone={onDone}
                        //showSkipButton={true}
                        //onSkip={onSkip}
                        renderDoneButton={this._renderDoneButton}
                        renderNextButton={this._renderNextButton}

                    />
                )}
        </>
    );
};

export default AppIntro;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        //padding: 10,
        justifyContent: "flex-end",
    },
    titleStyle: {
        padding: 10,
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
    },
    paragraphStyle: {
        padding: 20,
        textAlign: 'center',
        fontSize: 16,
    },
    introImageStyle: {
        width: 200,
        height: 200,
    },
    introTextStyle: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        paddingVertical: 30,
    },
    introTitleStyle: {
        fontSize: 25,
        color: 'white',
        textAlign: 'center',
        marginBottom: 16,
        fontWeight: 'bold',
    },
    text: {
        textAlign: "center",
        color: "white",
        fontSize: 16
    },
    loginButton: {
        //width: "40%",
        //zIndex: 2,
        marginBottom: 30,
        backgroundColor: "#26265F",
        paddingHorizontal: 50,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        height: 50
    },
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .3)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonCircle2: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .8)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

const slides = [
    {
        key: 's1',
        text: 'How It Works?',
        image: require("../assets/1.png"),
        backgroundColor: 'white',
    },
    {
        key: 's2',
        text: 'Scan OR Code',
        image: require("../assets/2.png"),
        backgroundColor: 'white',
    },
    {
        key: 's3',
        text: 'Choose To Buy & Pay',
        image: require("../assets/3.png"),
        backgroundColor: 'white',
    },
    {
        key: 's4',
        text: 'Take Out Selected Water Can',
        image: require("../assets/5.png"),
        backgroundColor: 'white',
    },
    {
        key: 's5',
        text: 'Keep Back Empty Can & Clase Door',
        image: require("../assets/4.png"),
        backgroundColor: 'white',
    },
    {
        key: 's5',
        text: 'Take A Photo',
        image: require("../assets/6.png"),
        backgroundColor: 'white',
    },

];