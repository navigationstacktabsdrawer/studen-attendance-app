import { CheckBox, Content } from "native-base";
import React, { Component } from "react";
import { SafeAreaView } from "react-native";
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StyleSheet,
    ImageBackground,
    Dimensions, Animated, Keyboard, KeyboardAvoidingView
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ScrollView } from "react-native-gesture-handler";
import { CodeField, Cursor } from "react-native-confirmation-code-field";
const ScreenWidth = Dimensions.get("window").width;
const ScreenHeight = Dimensions.get("window").height;
import Toast from 'react-native-tiny-toast';
import { Loader } from "../components/Loader";
import config from "../config/config";
import ResponsiveSize from "../config/ResponsiveSize";
import { loginDetail } from "../actions/Detail";
import { connect } from "react-redux";
import { CustomHeader } from "../components/CustomHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
const ToastData = {
    position: Toast.position.TOP,
    imgSource: require('../assets/alert.png'),
    imgStyle: { width: 40, height: 40 },
};
class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: "",
            Mobile: "",
            InstituteCode: "",
            ShowInstituteCode: false,
            //InstituteCode: "SPUNI001",
            //Email: "abc@gmail.com",
            //Mobile: "9814469661",
            //InstituteCode: "SPUNI001",
            Loader: false,
            Token: "",
            secureTextEntry: true
        };
    }

    storeData = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
            console.log('key==' + key + ', value==' + value);
        } catch (error) {
            // Error saving data
            alert('store data catched');
        }
    };
    componentDidMount() {
        //this.retrieveData();
    }
    retrieveData = async key => {
        try {
            const value = await AsyncStorage.getItem('token');
            this.setState({ Loader: false }, function () {

            })
            if (value !== null) {
                // alert (value);
                this.setState({ Token: value, }, function () {
                    this.props.navigation.navigate("Login")
                });
            }
        } catch (error) {
            this.setState({ Loader: false })
            alert('Error retrieving data');
        }
    }

    onLoadGetInstitudeId = () => {
        var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        console.log(this.state.Mobile.match(phoneno));
        if (this.state.Mobile == '') {
            this.setState({ Loader: false }, function () {
                Toast.show('Mobile number required', ToastData);
            })
        } else if (this.state.Mobile.match(phoneno) == null) {
            this.setState({ Loader: false }, function () {
                Toast.show('Enter valid Mobile number', ToastData);
            })
            //} else if (this.state.Email == '' || reg.test(this.state.Email) === false) {
            //    this.setState({ Loader: false }, function () {
            //        Toast.show(
            //            this.state.Email == ''
            //                ? 'Email is required'
            //                : 'Enter valid email address',
            //            ToastData
            //        );
            //    })
        } else {
            const url = config.baseUrl + "usermanagment/IsInstituteMapped/" + this.state.Mobile;
            console.log(url);
            fetch(url, {
                method: 'GET',
                headers: {
                    "Accept": 'application/json',
                    'Content-Type': 'application/json',
                    //'Authorization': 'Bearer ' + this.state.Token
                },
                //body: JSON.stringify({
                //    "mpin": this.state.Mpin
                //}),
            })
                .then(response => response.json())
                .then(responseJson => {
                    //alert(JSON.stringify(responseJson))
                    console.log(responseJson);
                    if (responseJson.status == true) {
                        this.setState({ Loader: false, ShowInstituteCode: false, InstituteCode: responseJson.message }, function () {
                            this.submit()
                        })

                    } else if (responseJson.status == false) {

                        this.setState({ Loader: false, ShowInstituteCode: true }, function () {
                            Toast.show(responseJson.message ? responseJson.message : "Not fetching your Institute id .Please enter manually", ToastData);

                        })

                    } else {
                        this.setState({ Loader: false, ShowInstituteCode: false }, function () {
                            Toast.show(responseJson.message ? responseJson.message : 'Temporary error try again after some time', ToastData);
                        })
                    }
                })
                .catch(error => {
                    this.setState({ Loader: false }, function () {
                        //alert(JSON.stringify(error))
                        console.log(error);
                        Toast.show("error", ToastData);
                    })
                });
        }
    }

    submit = () => {
        console.log("code==", this.state.InstituteCode, "mob==", this.state.Mobile);
        this.props.navigation.navigate("GetProfile", {
            instituteCode: this.state.InstituteCode,
            //emailId: this.state.Email,
            mobileNumber: this.state.Mobile,
            hashKey: "asd"
        })

    };

    render() {
        return (
            <KeyboardAwareScrollView
                //style={{ backgroundColor: '#4c69a5' }}
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={{ height: ScreenHeight, display: "flex" }}
                scrollEnabled={true}
            >
                <Loader
                    Loading={this.state.Loader ? true : false}
                />
                <SafeAreaView style={styles.mainContainer}>
                    <View style={styles.ImageConatiner}>
                        <Image style={{ width: "100%", height: "100%", resizeMode: "cover" }} source={require("../assets/createPinLogo.png")} />
                    </View>
                    <View style={styles.emailConatiner}>
                        <Text style={{ color: config.themeColor, width: "60%", textAlign: "left", marginLeft: 20, fontSize: ResponsiveSize(config.textSize) }}>Enter phone number for verification</Text>
                        <Text style={{ color: config.themeColor, width: "60%", textAlign: "left", marginLeft: 20, marginBottom: 10, fontSize: ResponsiveSize(11), lineHeight: 20, marginTop: 10, color: "#6397F2" }}>Please enter your phone number to receive a verification code to login</Text>
                        <View style={[styles.emailOuter, { marginTop: 30 }]}>
                            <View style={{ width: 52, height: 52, borderRadius: 52, justifyContent: "center", alignItems: "center", borderWidth: 2, borderColor: "white", elevation: 5, shadowColor: "gray", shadowOffset: { width: 1, height: 1 }, shadowOpacity: .6 }}>
                                <Image style={{ width: 50, height: 50, resizeMode: "cover", borderRadius: 50 }} source={require("../assets/flag.png")} />
                            </View>
                            <View style={styles.emailInputOuter}>
                                <TextInput
                                    style={styles.emailInput}
                                    onChangeText={text => this.setState({ Mobile: text }, function () { if (this.state.Mobile.length == 10) { this.setState({ Loader: true }, function () { this.onLoadGetInstitudeId() }) } })}
                                    value={this.state.Mobile}
                                    selectionColor="white"
                                    placeholder="Phone Number"
                                    returnKeyType="next"
                                    blurOnSubmit={true}
                                    autoFocus={true}
                                    //onSubmitEditing={() => {
                                    //    this.refs.input2.focus()
                                    //}}
                                    maxLength={10}
                                    returnKeyType="done"
                                    placeholderTextColor="silver"
                                    keyboardType="phone-pad" />
                                <TouchableOpacity onPress={() => {
                                    this.setState({ Mobile: "", ShowInstituteCode: false, InstituteCode: "" })
                                }}>
                                    <Image style={{ width: 20, height: 20, resizeMode: "cover", borderRadius: 25, }} source={require("../assets/cross2.png")} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        {this.state.ShowInstituteCode ?
                            <View style={[styles.emailOuter, { width: "87%", flexDirection: "column", alignItems: "flex-start", marginTop: 10 }]}>
                                <Text style={{ color: config.themeColor, textAlign: "left", fontSize: ResponsiveSize(12), lineHeight: 20, marginTop: 10, color: config.themeColor, marginBottom: 10, fontWeight: "bold" }}>Institide id</Text>

                                <CodeField

                                    {...this.props}
                                    value={this.state.InstituteCode}
                                    onChangeText={(text) => this.setState({ InstituteCode: text }, function () { console.log(this.state.InstituteCode); })}
                                    cellCount={6}
                                    blurOnSubmit={true}
                                    rootStyle={styles.codeFieldRoot}
                                    keyboardType="default"
                                    textContentType="oneTimeCode"
                                    renderCell={({ index, symbol, isFocused }) => (
                                        <View key={Math.random()} style={{ backgroundColor: config.SubThemeColor, borderRadius: 10 }}>
                                            <Text
                                                key={index}
                                                style={[styles.cell, isFocused && styles.focusCell]}
                                            >
                                                {(isFocused ? <Cursor /> : null),
                                                    symbol ? symbol : null
                                                }
                                            </Text>
                                        </View>
                                    )}
                                />
                            </View>
                            :
                            <View style={[styles.emailOuter, { width: "87%", flexDirection: "column", alignItems: "flex-start" }]}></View>

                        }
                        <TouchableOpacity style={[styles.loginButton, { marginTop: this.state.ShowInstituteCode ? 40 : 10, alignSelf: "center" }]} onPress={() =>
                            this.setState({ Loader: true }, function () {
                                var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;

                                if (this.state.Mobile == '') {
                                    this.setState({ Loader: false }, function () {
                                        Toast.show('Mobile number required', ToastData);
                                    })
                                } else if (this.state.Mobile.match(phoneno) == null) {
                                    this.setState({ Loader: false }, function () {
                                        Toast.show('Enter valid Mobile number', ToastData);
                                    })
                                } else if (!this.state.ShowInstituteCode && this.state.Mobile && this.state.InstituteCode == "") {
                                    this.onLoadGetInstitudeId()
                                } else {
                                    this.setState({ Loader: false }, function () {
                                        //if (this.state.InstituteCode.length != 6) {
                                        if (this.state.InstituteCode.length != 6) {
                                            this.setState({ Loader: false }, function () {
                                                Toast.show("Enter valid Institution Id", ToastData);
                                            })
                                        } else {
                                            this.submit()
                                        }
                                    })
                                }
                            })
                        }>
                            <Text style={styles.text}>Next</Text>
                        </TouchableOpacity>
                        <Text style={{ color: config.themeColor, width: "80%", alignSelf: "center", textAlign: "center", fontSize: ResponsiveSize(11), marginTop: 20, color: "#6397F2" }}>By providing my phone number, I hereby agree and accept the <Text style={{ color: "black", textDecorationLine: "underline" }}>Terms of Service</Text> and <Text style={{ color: "black", textDecorationLine: "underline" }}>Privacy Policy</Text> in use of the app.</Text>

                    </View>
                </SafeAreaView>
            </KeyboardAwareScrollView >
        )
    }
}
const styles = StyleSheet.create({
    ImageConatiner: {
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
        height: "40%",
    },
    mainContainer: {
        width: "100%",
        flex: 1,
        alignItems: "center",
        backgroundColor: config.bgColor
    },
    text: {
        textAlign: "center",
        color: "white",
        fontSize: ResponsiveSize(config.buttonSize)
    },
    logoConatiner: {
        width: "100%",
        height: "35%",
        justifyContent: "center",
        alignItems: "center"
    },
    bannerLogoConatiner: {
        width: "100%",
        height: "25%",
        justifyContent: "center",
        alignItems: "center"
    },
    loginText: {
        //color: "white",
        fontWeight: "600",
        fontSize: 35
    },
    emailConatiner: {
        width: "100%",
        height: "60%",
    },
    emailOuter: {
        width: "90%",
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        minHeight: 55,
        borderRadius: 5,
    },
    emailInput: {
        height: 55,
        width: "90%",
        fontSize: ResponsiveSize(18),
        paddingLeft: 10,
        textAlign: "center",
        backgroundColor: config.SubThemeColor,
        color: "white",
        borderRadius: 35,
    },
    emailInputOuter: {
        height: 55,
        width: "80%",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        backgroundColor: config.SubThemeColor,
        borderRadius: 35,
    },
    loginButton: {
        width: "45%",
        backgroundColor: config.themeColor,

        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        height: 45
    },
    buttonsContainer: {
        width: "100%",
        alignItems: "center",
        justifyContent: "space-evenly",
        flexDirection: "row",
        height: 55,
    },
    codeFieldRoot: {
        //marginTop: 90,
        width: "80%",
        alignSelf: "center"
    },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 40,
        fontSize: ResponsiveSize(config.textSize),
        borderWidth: 2,
        borderRadius: 10,
        color: "white",
        borderColor: '#00000030',
        textAlign: 'center',
    },
    focusCell: {
        shadowColor: "black",
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 1,
        color: "white",
        borderColor: "white",
        elevation: 2,
        borderRadius: 10,
    },
});
const mapStateToProps = state => {
    console.log(state);
    return {
        //  Details: state.detailReducer.DetailList
    };
};

const mapDispatchToProps = (dispatch) => {
    //console.log("dispatch====" + dispatch);
    return {
        //delete: (key) => dispatch(deleteDetail(key))
        loginDetail: (key) => dispatch(loginDetail(key))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
