import React, { Component } from 'react';
import { Thumbnail, Card, Header } from 'native-base';
import { Text, View, TouchableOpacity, StatusBar } from 'react-native';
import ResponsiveSize from '../config/ResponsiveSize';
import config from '../config/config';
export class CustomHeader extends Component {
    render() {
        let { navigation, isHome, title, notification } = this.props
        return (
            <View style={{ height: 50, flexDirection: "row", borderBottomColor: "silver", borderBottomWidth: .5, backgroundColor: "transparent", alignItems: "center" }}>
                <StatusBar barStyle="dark-content" backgroundColor="transparent" ></StatusBar>
                {isHome ?
                    <TouchableOpacity
                        onPress={() => { navigation.openDrawer() }}
                        style={{ width: "15%", justifyContent: "center", alignItems: "center" }}>
                        <Thumbnail square style={{ width: 25, height: 25, resizeMode: "contain" }} source={require("../assets/menuIcon.png")}
                        />
                    </TouchableOpacity>
                    :
                    (isHome == null ?
                        <View style={{ width: "15%", justifyContent: "center", alignItems: "center" }}>
                            {/*<Thumbnail square style={{ width: 40, height: 40, resizeMode: "cover" }} source={require("../assets/logoDemo.png")}></Thumbnail>*/}
                        </View>
                        :
                        <TouchableOpacity style={{ width: "15%", justifyContent: "center", alignItems: "center" }}
                            onPress={() => { navigation.goBack() }}>
                            <Thumbnail square style={{ width: 22, height: 22, resizeMode: "contain" }} source={require("../assets/back2.png")}
                            />
                        </TouchableOpacity>
                    )}
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ alignSelf: "center", fontWeight: "400", fontSize: ResponsiveSize(config.headerTextSize) }}>{title}</Text>
                </View>
                {notification ?
                    <TouchableOpacity style={{ width: "15%", justifyContent: "center", alignItems: "center" }}
                        onPress={() => { alert("notification") }}>
                        <Thumbnail square style={{ width: 40, height: 40, resizeMode: "contain" }} source={require("../assets/bell.png")}
                        />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity disabled style={{ width: "15%", justifyContent: "center", alignItems: "center" }}>
                    </TouchableOpacity>
                }
            </View>
        );
    }
}