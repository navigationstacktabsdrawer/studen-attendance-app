import React, { Component, useState } from 'react';
import { SafeAreaView, Text, StyleSheet, View, Dimensions } from 'react-native';
import PureChart from 'react-native-pure-chart';
import { CodeField, Cursor } from 'react-native-confirmation-code-field';
import config from '../config/config';
import ResponsiveSize from '../config/ResponsiveSize';
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";

const styles = StyleSheet.create({
    //chartConfig = {
    //    backgroundGradientFrom: "#1E2923",
    //    backgroundGradientFromOpacity: 0,
    //    backgroundGradientTo: "#08130D",
    //    backgroundGradientToOpacity: 0.5,
    //    //color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    //    strokeWidth: 2, // optional, default 3
    //    barPercentage: 0.5,
    //    useShadowColorFromDataset: false // optional
    //}
});
const height = 256;
const { width } = Dimensions.get("window");

const graphStyle = {
    marginVertical: 8,
    borderRadius: 16
};
const chartConfigs =
{
    backgroundColor: "#ff3e03",
    backgroundGradientFrom: "#ff3e03",
    backgroundGradientTo: "#ff3e03",
    color: (opacity = 1) => `rgba(${0}, ${0}, ${0}, ${opacity})`
}

export default class Test extends Component {
    constructor(props) {
        super(props);
        this.state = { value: "" }
    }
    render() {
        const pieChartData = [
            {
                name: "Seoul",
                population: 21500000,
                color: "rgba(131, 167, 234, 1)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 15
            },
            {
                name: "Toronto",
                population: 2800000,
                color: "#F00",
                legendFontColor: "#7F7F7F",
                legendFontSize: 15
            },
            {
                name: "Beijing",
                population: 527612,
                color: "red",
                legendFontColor: "#7F7F7F",
                legendFontSize: 15
            },
            {
                name: "New York",
                population: 8538000,
                color: "green",
                legendFontColor: "#7F7F7F",
                legendFontSize: 15
            },
            {
                name: "Moscow",
                population: 11920000,
                color: "rgb(0, 0, 255)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 15
            }
        ];
        const sampleData = ["100", "20", "30", "40", "50", "60", "700"]
        return (
            <View style={{ width: "100%", height: "100%", backgroundColor: "white", justifyContent: "center", alignItems: "center" }}>
                {/*<PureChart data={sampleData} type='pie' />*/}
                <Text>Bezier Line Chart</Text>
                <BarChart
                    data={{
                        labels: ["January", "February", "March", "April", "May", "June"],
                        datasets: [
                            {
                                data: sampleData

                            }
                        ]
                    }}
                    width={Dimensions.get("window").width - 20} // from react-native
                    height={220}
                    //yAxisLabel="$"
                    //yAxisSuffix="k"
                    yAxisInterval={1} // optional, defaults to 1
                    chartConfig={{
                        backgroundColor: "#e26a00",
                        backgroundGradientFrom: "#fb8c00",
                        backgroundGradientTo: "#ffa726",
                        decimalPlaces: 2, // optional, defaults to 2dp
                        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                        style: {
                            borderRadius: 16
                        },
                        propsForDots: {
                            r: "6",
                            strokeWidth: "2",
                            stroke: "#ffa726"
                        }
                    }}
                    bezier
                    style={{
                        marginVertical: 8,
                        borderRadius: 16
                    }}
                />
                <PieChart
                    data={pieChartData}
                    height={height}
                    width={width}
                    chartConfig={chartConfigs}
                    accessor="population"
                    style={graphStyle}
                    backgroundColor="transparent"
                    paddingLeft="15"
                />
            </View>
        );
    };
}