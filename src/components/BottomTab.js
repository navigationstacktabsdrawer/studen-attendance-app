import React, { Component } from "react";
import { Dimensions, TextInput, TouchableOpacity, KeyboardAvoidingView, View, Text, SafeAreaView, Image } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { SimpleAnimation } from 'react-native-simple-animations';
import config from "../config/config";
const ScreenHeight = Dimensions.get("window").height;
const ScreenWidth = Dimensions.get("window").width;
export default class BottomTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Home: true, active: "home"
        }
    }

    render() {
        const { navigation, active } = this.props;
        return (
            <View style={{ width: "95%", alignSelf: "center", height: 90, borderRadius: 20, backgroundColor: config.BottomTabBarColor, flexDirection: "row", alignItems: "center", justifyContent: "space-between", zIndex: 2 }}>
                <TouchableOpacity onPress={() => {
                    navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {/*<View style={{ width: 40, height: 40, marginTop: -20, justifyContent: "center", alignItems: "center", backgroundColor: "white", borderRadius: 50 }}>
                        <View style={{ width: 15, height: 15, backgroundColor: "#23C4D7", borderRadius: 20 }}></View>
                    </View>*/}
                    {active == "home" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/homeFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/home.png")} />
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {active == "subject" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/subjectFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/subject.png")} />
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {active == "schedule" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/scheduleFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/schedule.png")} />
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    navigation.navigate("DetailScreen")
                }} style={{ width: "25%", height: "100%", justifyContent: "center", alignItems: "center" }}>
                    {active == "result" ?
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/resultFilled.png")} />
                        :
                        <Image style={{ width: 50, height: 50, resizeMode: "contain" }} source={require("../assets/result.png")} />
                    }
                </TouchableOpacity>

            </View>
        )
    }
}